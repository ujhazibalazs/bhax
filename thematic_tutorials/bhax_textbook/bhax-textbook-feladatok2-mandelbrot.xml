<chapter xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xi="http://www.w3.org/2001/XInclude" version="5.0" xml:lang="hu">
    <info>
        <title>Helló, Mandelbrot!</title>
        <keywordset>
            <keyword/>
        </keywordset>
    </info>

    <section>
        <title>Reverse engineering UML osztálydiagram</title>
        <para>
            UML osztálydiagram rajzolása az első védési C++ programhoz. 
            Az osztálydiagramot a forrásokból
            generáljuk (pl. Argo UML, Umbrello, Eclipse UML) 
            Mutassunk rá a kompozíció és aggregáció
            kapcsolatára a forráskódban és a diagramon, 
            lásd még: https://youtu.be/Td_nlERlEOs. 
            https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog1_6.pdf (28-32 fólia)
        </para>
        <para>
        	Megoldás forrása: <link xlink:href="https://gitlab.com/ujhazibalazs/bhax/blob/master/thematic_tutorials/prog2_src/Mandelbrot/z3a7.cpp">Binfa</link>
        </para>

        <para>
        	Az első védési program a binfa volt. A forráskódját megtaláljuk az UDPROG Sourceforge repoban, vagy a fenti linken.
        </para>
        <para>
            Az osztálydiagram generálásához (reverse engineering) a Visual Paradigm programot használtam. 
            A program letölthető Windowsra és Linuxra is az oldalukról. 
            Telepítés után csak annyi dolgunk van, hogy kiválasztjuk a 
            fenti menüpontok közül, a Tools-t. A Tools-on belül pedig 
            a Code-ra kattintunk, majd Instant Reverse.
        </para>
        <para>
            A megnyíló kis ablak tetején beállítjuk a nyelvet "C++ / C# Source"-ra. 
            Az elérési út kiválasztásánál a fájl típust át állítjuk C++ Header File-ról 
            C++ Source File-ra, majd kiválasztjuk a z3a7.cpp-t. Végül az OK gombra 
            kattintva meg is történik a forráskódbol való osztálydiagram generálás 
            (reverse engineering).
        </para>
        <para>
             <mediaobject>
                <imageobject>
                    <imagedata fileref="mandelbrot/binfaosztalydiagram.png" scale="50" />
                </imageobject>
                <textobject>
                    <phrase>Az osztálydiagram</phrase>
                </textobject>
            </mediaobject>
        </para>
        <para>
            Aggregáció: részkapcsolat, egy részosztály több befogadó 
            osztályhoz tartozhat, van értelmük külön.
        </para>
        <para>
            Kompozíció: részosztály, csak az egész osztállyal együtt értelmezhető. 
            Ha törlésre kerül, az egész törlődik, mert önmagukban nincs értelmük.
        </para>
    </section>

    <section>
        <title>Forward engineering UML osztálydiagram</title>
        <para>
            UML-ben tervezzünk osztályokat és generáljunk belőle forrást!
        </para>
        <para>
            Megoldás forrása: <link xlink:href="https://gitlab.com/ujhazibalazs/bhax/blob/master/thematic_tutorials/prog2_src/Mandelbrot/z3a7.cpp">Binfa</link>
        </para>
        
        <para>
            Az első feladatban elkészült osztálydiagramból generáltam kódot. 
            A program, amit használtam megint a Visual Paradigm.
        </para>
        <para>
            A programban a felső menüpontból kiválasztjuk a Tools-t. 
            Itt a Code-ra kattintunk és ott kiválasztjuk azt, hogy 
            Instant Generator. A megnyíló ablak tetején a nyelvet beállítjuk 
            C++-ra és kijelöljük a diagramok közül azt, amelyikből kódot szeretnénk 
            generálni. Az output path jelöli azt az elérési utat, ahova kerülnek a 
            generált kódok. Ajánlott egy üres mappát készíteni és ezt beállítani. 
            A template directory maradjon az alapértelmezett elérési út, 
            az én esetemben ez a következő:
        </para>
        <programlisting language="tex">
            <![CDATA[
                /home/ujhazi/Visual_Paradigm_16.0/resources/instantgenerator/cplusplus
            ]]>
        </programlisting>
        <para>
            A Preview gombra kattintva lehet generálás előtt megnézni, hogy minden 
            rendben van-e. Itt láthatjuk a fájlokat amiket létre fog hozni és ha 
            rákattintunk egy-egy fájlra, akkor a tartalmát is megnézhetjük. 
            Ha mindent leellenőriztünk, akkor mehet a generálás a Generate gombbal. 
            Az Open Output Folder gombbal pedig megnyithatjuk azt a mappát, ahova 
            generálódni fognak a kódok.
        </para>
        <para>
            Generálás után így néz ki a mappa tartalma:
        </para>
        <para>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="mandelbrot/forwardgenerate.png" scale="40" />
                </imageobject>
                <textobject>
                    <phrase>forward engineering mappa tartalom</phrase>
                </textobject>
            </mediaobject>
        </para>
    </section>        
    
    <section>
        <title>BPMN</title>
        <para>
            Rajzoljunk le egy tevékenységet BPMN-ben!
            https://arato.inf.unideb.hu/batfai.norbert/UDPROG/deprecated/Prog2_7.pdf (34-47 fólia)
        </para>
        
        <para>
            A Camunda Modeler programot használtam a BPMN modell készítéséhez. 
            A tevékenység, amit ábrázoltam: pizza rendelés.
        </para>
        <para>
             <mediaobject>
                <imageobject>
                    <imagedata fileref="mandelbrot/pizzarendeles.png" scale="30" />
                </imageobject>
                <textobject>
                    <phrase>Pizza rendelés BPMN modell</phrase>
                </textobject>
            </mediaobject>
        </para>
        <para>
            A képen látható, hogy 3 esemény van összesen. 
            Az első a pizza rendelésének eseménye, azaz a vevőé. Addig várunk ameddig nem leszünk éhesek, ha éhesek lettünk, 
            akkor keresünk egy megfelelő pizzériát és felhívjuk. Addig próbálkozunk a hívással ameddig nem lesz sikeres. Ha felveszik a telefont, akkor megrendeljük az ételt, majd várunk arra, hogy megérkezzen. 
            Ha megérkezik, akkor kifizetjük és átvesszük, végül elfogyasztjuk.
        </para>
        <para>
            A második esemény a pizzéria szemszögéből történik. A rendelés felvétele úgy történik, 
            hogy ha van bejövő hívás, akkor azt fogadja, majd felveszi a rendelést. Ez után elkészíti 
            a kiszállítandó ételt, és ezt átadja a futárnak.
        </para>
        <para>
            A harmadik és egyben utolsó esemény a futár szemszöge. Itt történik a pizza kiszállítása. 
            Addig várunk, ameddig nem lesz kiszállítandó étel. Ha van étel, amit ki kell szállítani, 
            akkor átvesszük és kiszállítjuk. Végül átvesszük a pénzt és átadjuk az ételt.
        </para>
    </section>
    
    <section>
        <title>TeX UML</title>
        <para>
            Valamilyen TeX-es csomag felhasználásával készíts szép diagramokat az OOCWC projektről (pl.
            use case és class diagramokat).
        </para>
        <para>
        	Megoldás forrása: <link xlink:href="https://github.com/nbatfai/robocar-emulator">OOCWC</link>
        	Megoldás forrása: <link xlink:href="https://gitlab.com/ujhazibalazs/bhax/blob/master/thematic_tutorials/prog2_src/Mandelbrot/oocwc.mp">MetaUML</link>
        </para>

        <para>
        	Az OOCWC (Robocar World Championship) projektről a MetaUML segítségével készítettem 
            osztálydiagrammot.
        </para>
        <para>
            <mediaobject>
                <imageobject>
                    <imagedata fileref="mandelbrot/oocwc.pdf" scale="30" />
                </imageobject>
                <textobject>
                    <phrase>OOCWC class diagram</phrase>
                </textobject>
            </mediaobject>
        </para>
        <para>
            A MetaUML-ben írt fájlok kiterjesztése a .mp. Ahhoz, hogy ebből a forrásból diagram legyen, át kell fordítanunk pdf-re. 
            Az "mptopdf" parancs fog nekünk ebben segíteni. Használata jelen esetben:
        </para>
        <para>
             <mediaobject>
                <imageobject>
                    <imagedata fileref="mandelbrot/mptopdf.png" scale="30" />
                </imageobject>
                <textobject>
                    <phrase>mptopdf futtatása</phrase>
                </textobject>
            </mediaobject>
        </para>
        <para>
            Egy kis idő után a pdf-ünk el is készül, ez látható a fenti képen.
        </para>
    </section>
</chapter>                
