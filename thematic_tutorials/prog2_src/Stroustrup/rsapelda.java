import java.io.*;
import java.awt.event.*;
import java.awt.*;

public class rsapelda {
	public static void main(String[] args) throws IOException {
		KulcsPar jSzereplo = new KulcsPar();
		String cleantext = "Miusov, as a man man of breeding and deilcacy, could not but feel some inwrd qualms, when he reached the Father Superior's with Ivan: he felt ashamed of havin lost his temper. He felt that he ought to have disdaimed that despicable wretch, Fyodor Pavlovitch, too much to have been upset by him in Father Zossima's cell, and so to have forgotten himself. he reflceted, on the steps. And if they're decent people here (and the Father Superior, I understand, is a nobleman) why not be friendly and courteous withthem? I won't argue, I'll fall in with everything, I'll win them by politness, and show them that I've nothing to do with that Aesop, thta buffoon, that Pierrot, and have merely been takken in over this affair, just as they have.";

		PrintWriter writer = new PrintWriter("output.txt");

		//ENCRYPT
		//byte[] buffer = cleantext.getBytes();
		//java.math.BigInteger[] titkos = new java.math.BigInteger[buffer.length];

		//Karakterenként kódolás
		for(int idx = 0; idx < cleantext.length(); ++idx) {
			String tisztaszoveg = cleantext.substring(idx, idx + 1);
			tisztaszoveg = tisztaszoveg.toLowerCase();
			byte[] buffer = tisztaszoveg.getBytes();
			java.math.BigInteger[] titkos = new java.math.BigInteger[buffer.length];
			byte[] output = new byte[buffer.length];

			for (int i=0;i<titkos.length;i++) {
				titkos[i] = new java.math.BigInteger(new byte[]{buffer[i]});
				titkos[i] = titkos[i].modPow(jSzereplo.e,jSzereplo.m);
				//karakterenként kódolás
				output[i] = titkos[i].byteValue();
				writer.print(titkos[i]);
			}
			writer.println();
		}

		//DECRYPT
		/*
		for (int i=0;i<titkos.length;i++) {
			titkos[i] = titkos[i].modPow(jSzereplo.d,jSzereplo.m);
			buffer[i] = titkos[i].byteValue();
		}

		System.out.println(new String(buffer));
		*/
	
		BufferedReader inputStream = new BufferedReader(new FileReader("output.txt"));

		int lines = 0;
		String line[] = new String[100000];

		while((line[lines] = inputStream.readLine()) != null) {
			lines++;
		}
		inputStream.close();

		Karakterek kar[] = new Karakterek[1000];
		boolean found;
		kar[0] = new Karakterek(line[0]);
		int count = 1;

		for(int i = 1; i < lines; i++) {
			found = false;
			for(int j = 0; j < count; j++) {
				if(kar[j].getEncrypted().equals(line[i])) {
					kar[j].increment();
					found = true;
					break;
				}
			}
			if(!found) {
				kar[count] = new Karakterek(line[i]);
				count++;
			}
		}

		for(int i = 0; i < count; i++) {
			for(int j = i + 1; j < count; j++) {
				if(kar[i].getGyak() < kar[j].getGyak() ) {
					Karakterek x = kar[i];
					kar[i] = kar[j];
					kar[j] = x;
				}
			}
		}

		FileReader f = new FileReader("gyakorisag.txt");
		char[] karakter = new char[80];
		int karCount=0;
		int k;

		while((k = f.read()) != -1) {
			if((char)k != '\n') {
				karakter[karCount] = (char)k;
				karCount++;
			}
		}
		f.close();

		for(int i = 0; i < count; i++) {
			kar[i].setKarakter(karakter[i]);
		}

		for(int i = 0; i < lines; i++) {
			for(int j = 0; j < count; j++) {
				if(line[i].equals(kar[j].getEncrypted())) {
					System.out.print(kar[j].getKarakter());
				}
			}
		}
		System.out.println();
	}
}

class KulcsPar {
	java.math.BigInteger d,e,m;
	public KulcsPar() {
		int meretBitekben = 700*(int)(java.lang.Math.log((double) 10) / java.lang.Math.log((double) 2));
		//System.out.println("Méret bitekben: " + meretBitekben);
		java.math.BigInteger p = new java.math.BigInteger(meretBitekben, 100, new java.util.Random());
		//System.out.println("p: " + p);
		//System.out.println("p hexa: " + p.toString(16));
		java.math.BigInteger q = new java.math.BigInteger(meretBitekben, 100, new java.util.Random());
		//System.out.println("q: " + q);
		m = p.multiply(q);
		//System.out.println("m: " + m);
		java.math.BigInteger z = p.subtract(java.math.BigInteger.ONE).multiply(q.subtract(java.math.BigInteger.ONE));
		//System.out.println("z: " + z);
		do {
			do {
				d = new java.math.BigInteger(meretBitekben, new java.util.Random());
			} while(d.equals(java.math.BigInteger.ONE));
		} while(!z.gcd(d).equals(java.math.BigInteger.ONE));
		//System.out.println("d: " + d);
		e = d.modInverse(z);
		//System.out.println("e: " + e);
	}
}

class Karakterek{
	private String encrypted;
	private char karakter = ' ';
	private int gyakorisag = 0;

	public Karakterek(String str, char k){
		encrypted = str;
		karakter = k;
	}

	public Karakterek(String str){
		encrypted = str;
	}

	public void setEncrypted(String str){
		encrypted = str;
	}

	public String getEncrypted(){
		return encrypted;
	}

	public void setKarakter(char k){
		karakter = k;
	}

	public char getKarakter(){
		return karakter;
	}

	public void increment(){
		gyakorisag += 1;
	}

	public int getGyak(){
		return gyakorisag;
	}
}