#include <iostream>
#include <iterator>
#include <map>
#include <set>
#include <algorithm>
#include <functional>

using namespace std;

int main(int argc, char const *argv[])
{
	map<int, int> szamok;

	szamok.insert(pair<int, int>(1, 40));
	szamok.insert(pair<int, int>(2, 30));
	szamok.insert(pair<int, int>(3, 60));
	szamok.insert(pair<int, int>(4, 20));
	szamok.insert(pair<int, int>(5, 50));
	szamok.insert(pair<int, int>(6, 10));

	map<int, int>::iterator itr;

	cout << "\nKulcs szerint rendezve: \n";
	cout << "Kulcs\tÉrték\n";

	for (itr = szamok.begin(); itr != szamok.end(); itr++) {
		cout << itr->first << '\t' << itr->second << '\n';
	}

	typedef function<bool(pair<int, int>, pair<int, int>)> Comparator;

	Comparator comp = [](pair<int, int> elso, pair<int, int> masodik) {
		return elso.second < masodik.second;
	};

	set<pair<int, int>, Comparator> osszehas(szamok.begin(), szamok.end(), comp);

	cout << "\nÉrték szerint rendezve:\n";
	cout << "Kulcs\tÉrték\n";
	for (pair<int, int> element : osszehas) {
		cout << element.first << '\t' << element.second << '\n';
	}

	return 0;
}