using namespace std;
#include <iostream>
#include <cmath>
#include <fstream>
#include <vector>
#include <algorithm>

class LeetCypher
{
	public:
	LeetCypher();
	string decypher(string& text);
	private:
	vector <vector <string>> leet_alphabet;
	vector <string> eng_alphabet;
};

LeetCypher::LeetCypher()
{
	srand (time(NULL));
	leet_alphabet.resize(26);
	leet_alphabet[0]= {"4", "/-\\", "/_\\", "@", "/\\"};
	leet_alphabet[1]={"8","|3", "13", "|}", "|:", "|8", "18", "6", "|B", "|8", "lo", "|o"};
	leet_alphabet[2]= { "<", "{", "[", "("};
	leet_alphabet[3] = {"|)", "|}", "|]"};
	leet_alphabet[4] = {"3"};
	leet_alphabet[5] = { "|=", "ph", "|#", "|\""};
	leet_alphabet[6] = {"[", "-", "[+", "6"};
	leet_alphabet[7] = {"4", "|-|", "[-]", "{-}", "|=|", "[=]", "{=}"};
	leet_alphabet[8] = {"1", "|", "!", "9"};
	leet_alphabet[9] = {"_|", "_/", "_7", "_)", "_]", "_}"};
	leet_alphabet[10] = {"|<", "1<", "l<", "|{", "l{"};
	leet_alphabet[11] = {"|_", "|", "1", "]["};
	leet_alphabet[12] = {"44", "|\\/|", "^^", "/\\/\\", "/X\\", "[]\\/][", "[]V[]", "(V)", "//., .\\\\", "N\\"};
	leet_alphabet[13] = {"|\\|", "/\\/", "/V", "][\\\\]["};
	leet_alphabet[14] = {"0", "()", "[]", "{}", "<>"};
	leet_alphabet[15] = { "|o", "|O", "|>", "|*", "|\textdegree{}", "|D", "/o"};
	leet_alphabet[16] = {"O_", "9", "(,)", "0,"};
	leet_alphabet[17] = {"|2"," 12", ".-", "|^", "l2"};
	leet_alphabet[18] = {"5", "$", "§"};
	leet_alphabet[19] = {"7", "+", "7‘"};
	leet_alphabet[20] = {"|_|", "\\_\\", "/_/", "\\_/", "(_)", "[_]", "{_}"};
	leet_alphabet[21] = {"\\/"};
	leet_alphabet[22] = {"\\/\\/", "(/\\)", "\\^/", "|/\\|", "\\X/"};
	leet_alphabet[23] = {"%", "*", "><", "}{", ")("};
	leet_alphabet[24] = {"‘/"};
	leet_alphabet[25] = { "2", "7_", ">_"};

	eng_alphabet =
	{
		"A",
		"B",
		"C",
		"D",
		"E",
		"F",
		"G",
		"H",
		"I",
		"J",
		"K",
		"L",
		"M",
		"N",
		"O",
		"P",
		"Q",
		"R",
		"S",
		"T",
		"U",
		"V",
		"W",
		"X",
		"Y",
		"Z"
	};
}

string LeetCypher::decypher(string &text)
{
	string leet = "";
	string eng = text;
	transform(eng.begin(), eng.end(),eng.begin(), ::toupper);
	while(!eng.empty())
	{
		bool find=false;
		for (int i=0; i<eng_alphabet.size(); i++)
		{
			size_t found = eng.find(eng_alphabet[i]);
			if (found==0)
			{
				find =true;
				leet+=leet_alphabet[i][rand() % leet_alphabet[i].size()];
				if(eng.length() > 1)
				eng=eng.substr(1);
				else
				eng.clear();
			}
		}
		if(!find)
		{
			leet+=eng[0];
			if(eng.length()>1)
			eng=eng.substr(1);
			else
			eng.clear();
		}
	}
	return leet;
}

int main(int argc, char * argv[])
{
	ifstream infile (argv[1]);
	fstream outfile (argv[2], ios::out);
	string text = "";
	string temp = "";
	while(getline(infile, temp))
	{
		text+=temp;
	}
	LeetCypher* cypher = new LeetCypher();
	string cypheredText = cypher->decypher(text);
	outfile<<cypheredText<<endl;
}