#include <iostream>

class Rectangle
{
	public:
	int m_width;
	int m_height;

	virtual void setWidth(int width){
		m_width = width;
	}

	virtual void setHeight(int height){
		m_height = height;
	}

	int getWidth(){
	return m_width;
	}
	int getHeight(){
	return m_height;
	}
	int getArea(){
	return m_width * m_height;
	}
};

class Square : public Rectangle
{
	public:
	void setWidth(int width){
		m_width = width;
		m_height = width;
	}
	void setHeight(int height){
		m_width = height;
		m_height = height;
	}
};

Rectangle* getNewRectangle()
{
	return new Square();
}

int main ()
{
	Rectangle* r = getNewRectangle();
	r->setWidth(5);
	r->setHeight(10);
}