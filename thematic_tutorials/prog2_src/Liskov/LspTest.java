class Rectangle{
	protected int m_width;
	protected int m_height;

	public void setWidth(int width){
		m_width = width;
	}

	public void setHeight(int height){
		m_height = height;
	}

	public int getWidth(){
		return m_width;
	}

	public int getHeight(){
		return m_height;
	}

	public int getArea(){
		return m_width * m_height;
	}
}

class Square extends Rectangle{
	public void setWidth(int width){
		m_width = width;
		m_height = width;
	}

	public void setHeight(int height){
		m_width = height;
		m_height = height;
	}
}

class LspTest{
	private static Rectangle getNewRectangle(){
		return new Square();
	}

	public static void main(String[] args) {
		Rectangle r = LspTest.getNewRectangle();
		r.setWidth(5);
		r.setHeight(10);
		System.out.println(r.getArea());
	}
}